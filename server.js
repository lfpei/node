const express = require('express');
const path = require('path');
const hbs = require('hbs');
const fs = require('fs');
const app = express();

const port = process.env.PORT || 3000;
module.exports = app;


app.set('view engine', 'hbs');


hbs.registerPartials(__dirname + '/views/partials');
hbs.registerHelper('getCurrentYear', () => {
    return new Date().getFullYear();
});
hbs.registerHelper('capitalize', (text) => {
    return text.charAt(0).toUpperCase() + text.slice(1);

});

// app.use((request, response, next) => {
//
//     response.render('partials/maintenance.hbs')
//
// });

app.use(express.static(__dirname + '/public'));


app.use((request, response, next) => {

    let now = new Date().toDateString();

    console.log(`This is now ${now}, ${request.method}, ${request.url}`)

    next();
});



app.get('/', (request, response) => {
    response.render('about.hbs', {
        pageTitle: 'This is home',

    });
});

app.get('/about', (request, response) => {
    response.render('about.hbs', {
        pageTitle: 'about page',
    });

});


app.listen(port, () => {
    console.log(`Server is up on port ${port}`);
});



