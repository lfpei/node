let mongoose = require('mongoose');


let Item = mongoose.model('Item', {
    text: {type: String, required: true, minlength: 1, trim: true},
    completed: {type: Boolean, default: false}

});


module.exports = {Item: Item};