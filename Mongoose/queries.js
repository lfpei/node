let mongoose = require('mongoose');
mongoose.Promise = global.Promise;
let objectId = require('mongodb').objectId;

let Item = require('./Item').Item;
let User = require('./User').User;

let express = require('express');
let bodyParser = require('body-parser');
let app = express();

mongoose.connect('mongodb://localhost:27017/TodoApp');

